$('#slide-service .owl-carousel').owlCarousel({
	loop: false,
	nav: false,
	dots: true,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 18,
	// navText:[`<i class="fal fa-angle-left"></i>`, `<i class="fal fa-angle-right"></i>`],
	responsive: {
		0: {
			items: 1,
		},
		540: {
			items: 2
		}, 1024: {
			items: 3
		}
	}
});
$('#about-us .owl-carousel').owlCarousel({
	loop: false,
	nav: true,
	dots: false,
	autoplay: true,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 0,
	navText:[`<img src="../images/prev.png" />`, `<img src="../images/next.png" />`],
	responsive: {
		0: {
			items: 1,
		},
		540: {
			items: 2
		}, 1024: {
			items: 3
		}
	}
});
$('#list-rate .owl-carousel').owlCarousel({
	loop: true,
	nav: false,
	dots: true,
	autoplay: true,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 0,responsive: {
		0: {
			items: 2,
		},
		540: {
			items: 3
		}, 768: {
			items: 4
		}
	}
});


function resizeImage() {
	console.log(1);
	let arrClass = [
		{ class: 'resize-service', number: (217 / 388) },
		{ class: 'resize-image-cntt-1', number: (294 / 650) },
		{ class: 'resize-image-cntt-2', number: (395 / 658) },
		{ class: 'resize-gallery-1', number: (154 / 237) },
		{ class: 'resize-gallery-2', number: (311 / 237) },
		{ class: 'resize-gallery-3', number: (154 / 481) },
	];
	for (let i = 0; i < arrClass.length; i++) {
		if($("." + arrClass[i]['class']).length){
			let width = document.getElementsByClassName(arrClass[i]['class'])[0].offsetWidth;
			$("." + arrClass[i]['class']).css('height', width * arrClass[i]['number'] + 'px');
		}
	}
}

resizeImage();
new ResizeObserver(() => {
	resizeImage();
	
}).observe(document.body)

$('#list-rate').waypoint(function () {
	$(".counter").each(function () {
		var $this = $(this),
			countTo = $this.attr("data-countto");
		countDuration = parseInt($this.attr("data-duration"));
		$({ counter: $this.text() }).animate(
			{
				counter: countTo
			},
			{
				duration: countDuration,
				easing: "linear",
				step: function () {
					$this.text(Math.floor(this.counter));
				},
				complete: function () {
					$this.text(this.counter);
				}
			}
		);
	});
}, {
	offset: '100%'
});


$("#enrollment-method .box-method .title-tabs .item").click(function() {
	let idElement = $(this).attr("attr-link")
	$(this).siblings(".item").removeClass("active")
	$(this).addClass("active")
	$("#enrollment-method .box-method .box-contents .item-content").removeClass("active")
	$(`#enrollment-method .box-method .box-contents .item-content${idElement}`).addClass("active")
})
$('#categoryModal').on('show.bs.modal', function () {
	setTimeout(() => {
		let width = document.getElementsByClassName("resize-image-modal")[0].offsetWidth;
		console.log(width);
		$(".resize-image-modal").css('height', width * (463 / 341) + 'px');
	}, 1000);
})

$(document).ready(function () {

	// menu
	$(".toggle-menu").click(function(){
		$(this).toggleClass("active")
		$("body").toggleClass("active-menu")
		$(".wrap-menu").toggleClass("active")
	})
	$(document).mouseup(function (e) {
		var container = $("header#header");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
				$(".toggle-menu").removeClass("active")
				$("body").removeClass("active-menu")
				$(".wrap-menu").removeClass("active")
		}
	});
	// 
	
});